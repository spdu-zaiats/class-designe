package group.spd._1_inheritance;

public class CreditBankAccount extends BankAccount {

    private final int credit;

    public CreditBankAccount(int credit) {
        this.credit = credit;
    }

    @Override
    public int getAvailableAmount() {
        return getBalance() + credit;
    }

    public void test() {

    }
}
