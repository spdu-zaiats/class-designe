package group.spd._1_inheritance;

public class BankApp {

    public static void main(String[] args) throws NotEnoughMoneyException {
        final CreditBankAccount bankAccount = new CreditBankAccount(100);
        bankAccount.test();
        final BankApp bankApp = new BankApp();

        bankApp.deposit(bankAccount, 100);
        bankApp.deposit(bankAccount, 100);
        bankAccount.printBalance();
        bankApp.withdraw(bankAccount, 150);
        bankApp.withdraw(bankAccount, 100);
        bankApp.withdraw(bankAccount, 100);
    }

    public void deposit(BankAccount account, int amount) {
        System.out.printf("Balance before operation is %d. Add %d%n", account.getBalance(), amount);
        account.deposit(amount);
    }

    public void withdraw(BankAccount account, int amount) throws NotEnoughMoneyException {
        System.out.printf("Balance before operation is %d. Withdraw %d%n", account.getBalance(), amount);
        if (account.getAvailableAmount() < amount) {
            throw new NotEnoughMoneyException();
        }
        account.withdraw(amount);
    }
}
