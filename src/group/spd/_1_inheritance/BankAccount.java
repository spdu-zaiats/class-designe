package group.spd._1_inheritance;

import java.util.ArrayList;
import java.util.List;

public class BankAccount implements FinancialAccount, Test {

    private List<Transaction> transactions = new ArrayList<>();

    @Override
    public void deposit(int amount) {
        transactions.add(new Transaction(amount));
    }

    @Override
    public void withdraw(int amount) {
        transactions.add(new Transaction(amount * -1));
    }

    @Override
    public int getBalance() {
        FinancialAccount.super.printBalance();
        return transactions.stream()
                .mapToInt(Transaction::getAmount)
                .sum();
    }

    @Override
    public void printBalance() {
        FinancialAccount.super.printBalance();
        Test.super.printBalance();
    }

    public int getAvailableAmount() {
        return getBalance();
    }
}
