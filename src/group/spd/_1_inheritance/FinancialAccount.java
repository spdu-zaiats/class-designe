package group.spd._1_inheritance;

public interface FinancialAccount {

    void deposit(int amount);

    void withdraw(int amount);

    int getBalance();

    default void printBalance() {
        System.out.println(getBalance());
    }
}
