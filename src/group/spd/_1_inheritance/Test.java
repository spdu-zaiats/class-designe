package group.spd._1_inheritance;

public interface Test {
    default void printBalance() {
        System.out.println("Hello from test interface");
    }
}
