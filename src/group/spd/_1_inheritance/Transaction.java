package group.spd._1_inheritance;

import java.time.LocalDateTime;

public class Transaction {

    private final int amount;
    private final LocalDateTime date;

    public Transaction(int amount) {
        this.amount = amount;
        this.date = LocalDateTime.now();
    }

    public int getAmount() {
        return amount;
    }

    public LocalDateTime getDate() {
        return date;
    }
}
