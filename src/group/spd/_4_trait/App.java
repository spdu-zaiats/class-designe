package group.spd._4_trait;

public class App {

    public static void main(String[] args) {
        final TestClass testClass = new TestClass();
        testClass.printName();
        testClass.printDetails();
    }
}
