package group.spd._4_trait;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public interface MorozovTrait {

    default void printDetails() {
        System.out.println("========= Fields =========");
        for (Field field : getClass().getDeclaredFields()) {
            printFieldDetails(field);
        }

        System.out.println("========= Methods =========");
        for (Method method : getClass().getDeclaredMethods()) {
            System.out.println(method.getName());
        }
    }

    private void printFieldDetails(Field field) {
        field.setAccessible(true);
        try {
            System.out.printf("%s = %s%n", field.getName(), field.get(this));
        } catch (IllegalAccessException e) {
            System.out.println("Can't read value for field " + field.getName());
        }
    }
}
