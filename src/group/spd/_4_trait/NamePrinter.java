package group.spd._4_trait;

public interface NamePrinter {

    String getName();

    default void printName() {
        System.out.println(getName());
    }
}
