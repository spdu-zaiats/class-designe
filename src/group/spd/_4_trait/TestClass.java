package group.spd._4_trait;

public class TestClass implements NamePrinter, MorozovTrait {

    private final String name = "Some name";
    private final int num = 42;

    public void somePublicMethod() {
        System.out.println("Do nothing");
    }

    void somePackageMethod() {
        System.out.println("Do nothing");
    }

    private void somePrivateMethod() {
        System.out.println("Do nothing");
    }

    @Override
    public String getName() {
        return name;
    }
}
