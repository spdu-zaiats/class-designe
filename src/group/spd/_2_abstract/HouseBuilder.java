package group.spd._2_abstract;

public abstract class HouseBuilder {

    protected abstract void buildPillars();
    protected abstract void buildWalls();

    public void buildHouse() {
        buildFoundation();
        buildPillars();
        buildWalls();
        buildWindows();
        System.out.println("House is built");
    }

    private void buildFoundation() {
        System.out.println("Building foundation with cement, iron rods and sand");
    }

    private void buildWindows() {
        System.out.println("Building Glass Windows");
    }
}
