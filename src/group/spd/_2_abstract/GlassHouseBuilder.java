package group.spd._2_abstract;

public class GlassHouseBuilder extends HouseBuilder {
    @Override
    protected void buildPillars() {
        System.out.println("Building Pillars with glass coating");
    }

    @Override
    protected void buildWalls() {
        System.out.println("Building Glass Walls");
    }
}
