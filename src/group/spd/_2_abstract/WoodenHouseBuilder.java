package group.spd._2_abstract;

public class WoodenHouseBuilder extends HouseBuilder {
    @Override
    protected void buildPillars() {
        System.out.println("Building Pillars with Wood coating");
    }

    @Override
    protected void buildWalls() {
        System.out.println("Building Wooden Walls");
    }
}
