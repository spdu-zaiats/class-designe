package group.spd._2_abstract;

public class App {

    public static void main(String[] args) {
        final HouseBuilder houseBuilder = getHouseBuilder();
        houseBuilder.buildHouse();
    }

    private static HouseBuilder getHouseBuilder() {
        return new GlassHouseBuilder();
    }
}
