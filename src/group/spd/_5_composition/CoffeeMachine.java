package group.spd._5_composition;

public class CoffeeMachine {

    private Grinder grinder;
    private WaterTank waterTank;

    public CoffeeMachine(Grinder grinder, WaterTank waterTank) {
        this.grinder = grinder;
        this.waterTank = waterTank;
    }
}
